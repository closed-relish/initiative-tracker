# CHANGELOG

<!--- next entry here -->

## 0.1.7
2020-11-06

### Fixes

- **#17:** force update snapshots in test (8451810d1008549964549d140e513fd07673b64f)

## 0.1.6
2020-08-20

### Fixes

- **#15:** remove version from tag piplines (f02147193d331bd5b51acedff93ec40a89d27789)

## 0.1.5
2020-08-20

### Fixes

- **#15:** adding package files as artifacts (211fef10a6f603c88b98bad68b32fec22a255612)

## 0.1.4
2020-08-20

### Fixes

- **#15:** remove needs test:unit (53ca3490a786633fdee8dc082837c199d4750ccd)

## 0.1.3
2020-08-20

### Fixes

- **#15:** first draft at version incrememntation (3370ed569764a38399a68791065584ddc5f9bdaf)
- **#15:** minor update to DAG (534dddf4dffd41b3623b3e4fc1d427021b302fdb)
- **#15:** added reliance on test for version release (1e2975654a66bbbcc21edaf8dfd8b410e130c3e1)
- **#15:** removed duplicate need requirement (f53065b3f5b9226f30c4ba3f6765c1cadd4f39d9)

## 0.1.2
2020-08-16

### Fixes

- **#14:** ensure coverage report is in correct format (d3184025befd02933f43744875887a7d92a0168c)
- **#14:** updating pipelines to use needs over dependiences (436e189ca6a3db4bdb7a93ec3cad957116b0cb62)

## 0.1.1
2020-08-16

### Fixes

- **#13:** updated pipeline-ci to try and fix page deployments (f6546dd3f63c17bd74baac9af1ac94ef8553697e)
- **#13:** adding tag pipeline flag for pages deployment. (507c699005aef4bb415eb5408ee5284a08af5c08)

## 0.1.0
2020-08-16

### Features

- **#6:** started base react app (5ac315e8bc9a1a192f7f7b3bd54a9b24f17f88be)

### Fixes

- stop auto release on master (3ac916360148d71ab282aa0d473e506ca8cd2c0d)
- updated README (afc2cec08efbf68b629d4e8311330a1ccba601da)
- attempting pipeline refactor (37d2b6bd90710936774e1fd5cb007d1210dda111)
- updated pipeline test suite and packages (5191bea0b9888fef935385c15da1f2fa4858a96b)
- dockerized app (5ca5a20c04d90a37bc61023ed520b83b5fd44e94)
- removed coverage command from pipeline - not required. (722c185ab83f4cd397b1dcdf3917592e46508b94)
- added updates from MR comments (78d7afc42c698dbd9087d717648845fb9d84fae5)

## 0.0.6
2020-08-12

### Fixes

- updated README & pipelines (a9391e3053e59ce393e8d5c788b3f4f1be40e2bb)