# Contributing

## Issues

Issues are very valuable to this project.

* Ideas are a valuable source of contributions others can make
* Problems show where this project is lacking
* With a question you show where contributors can improve the user experience

Thank you for creating them.

## Merge Requests

Merge requests are a great way to get your ideas into this repository.

When deciding if we merge in a request we will look at the following things:

### Does it state intent

You should be clear which problem you're trying to solve with your contribution.

For example:

> Add link to code of conduct in README.md

Doesn't tell us anything about why you're doing that

> Add link to code of conduct in README.md because users don't always look in the CONTRIBUTING.md

Tells us the problem that you have found, and the merge request shows us the action you have taken to solve it.


### Is it of good quality

* There are no spelling mistakes
* It reads well
* For english language contributions: Has a good score on [Grammarly](grammarly.com) or [Hemingway App](http://www.hemingwayapp.com/)
* This repository follows [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary) to enable ease of versioning. Please do your best to follow this standard in your commit messages.

### Does it move this repository closer to my vision for the repository

The aim of this repository is:

* To provide a tool for Dungeon / Game Masters which _should_ enable them to speed up 5e combat.
* Foster a culture of respect and gratitude in the open source community.

### Does it follow the contributor covenant

This repository has a [code of conduct](CODE_OF_CONDUCT.md), This repository has a code of conduct, I will remove things that do not respect it.