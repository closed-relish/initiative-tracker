# Initiative Tracker

This project aims to create some useful tools for Dungeon / Game masters.

Specifically tracking Initiative orders for D&D 5e combat, but may be extended to allow for more game editions and games.

## Support

All support requests can be made in the form of Issues on the project, or via our dedicated [Discord](https://discord.gg/cv8wqkW) server

## Development Guide

### Requirements
* [Docker](https://www.docker.com/get-started)
* [docker-compose](https://docs.docker.com/compose/install/)

## Maintainers

* Lead Developer: @bassforce86
* Developer: @GilesWKing
